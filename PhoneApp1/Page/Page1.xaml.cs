﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
namespace PhoneApp1.Page
{
   
    public partial class Page1 : PhoneApplicationPage
    {
        DispatcherTimer timer = new DispatcherTimer();
        public Page1()
        {
            InitializeComponent();
            InAppbrowser.NavigateToString("<html>"+
                "<head>"+
                "<script type=\"text/javascript\">"+
                " function caculate(arg) {  try {var res = eval(arg).toString() ; return res; } catch (e) { return \"none\" ;}  }" + 
                "</script>"+
                "</head>"+ 
               "</html>");
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(timer_Tick);
           
        }
        void timer_Tick(object sender,EventArgs e)
        {
            timer.Stop();
            monitor.Text = "";
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
         
            var button = (sender as Button);
            String title = (String)button.Content; 
            switch (title)
            {
                case "sqrt":
                    monitor.Text += " Math.sqrt("; break;
                case "AC" :
                    monitor.Text = "";  break;
                default:
                    monitor.Text += title; break;

            }    
        }
        private void InappBrowser_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Loaded");
        }


        private void InappBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Load Complete");
        }
        private void Button_Equal_Click(object sender, RoutedEventArgs e)
        {
            var res = (string)InAppbrowser.InvokeScript("caculate", monitor.Text);
            if( res == "none" )
            {
                monitor.Text = "Biểu thức không hợp lệ ";
                timer.Start();
            }
            else
            {
                monitor.Text = res;
            }
            System.Diagnostics.Debug.WriteLine("K qua la: " + res);
            
        }

        private void InAppbrowser_Navigated_1(object sender, NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("navigate");
        }

        private void Button_BackSpace(object sender, RoutedEventArgs e)
        {
            monitor.Text = monitor.Text.Substring(0, monitor.Text.Length-1);
        }
    }
}