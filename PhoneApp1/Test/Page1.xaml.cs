﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PhoneApp1
{
    public partial class Page1 : PhoneApplicationPage
    {
        public Page1()
        {
            InitializeComponent();
            System.Diagnostics.Debug.WriteLine("Print something");
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            System.Diagnostics.Debug.WriteLine("co navigate toi");
            string msg = "";

            if (NavigationContext.QueryString.TryGetValue("id_book", out msg))
            {
                System.Diagnostics.Debug.WriteLine("msg ne " + msg);
            }     

        }
    }
}