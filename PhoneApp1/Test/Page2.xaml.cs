﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
namespace PhoneApp1
{
    public partial class Page2 : PhoneApplicationPage
    {
        public ObservableCollection<Recording> MyMusic = new ObservableCollection<Recording>();

        public class Recording
        {
            public Recording() { }
            public Recording(string artistName, string cdName, DateTime release)
            {
                Artist = artistName;
                Name = cdName;
                ReleaseDate = release;
            }
            public string Artist { get; set; }
            public string Name { get; set; }
            public DateTime ReleaseDate { get; set; }
            // Override the ToString method.
            public override string ToString()
            {
                return Name + " by " + Artist + ", Released: " + ReleaseDate.ToShortDateString();
            }
        }

        public Page2()
        {
            InitializeComponent();
            ListBox listBox1 = new ListBox();
            listBox1.Items.Add("Item 1");
            listBox1.Items.Add("Item 2");
            listBox1.Items.Add("Item 3");
            listBox1.Width = 140;
            listBox1.SelectionChanged += lstCountries_SelectionChanged;
            // Add the list box to a parent container in the visual tree.
            LayoutRoot.Children.Add(listBox1);
            textBox1.DataContext = new Recording("Chris Sells", "Chris Sells Live",
      new DateTime(2008, 2, 5));


            MyMusic.Add(new Recording("Chris Sells", "Chris Sells Live",
   new DateTime(2008, 2, 5)));
            MyMusic.Add(new Recording("Luka Abrus",
               "The Road to Redmond", new DateTime(2007, 4, 3)));
            MyMusic.Add(new Recording("Jim Hance",
               "The Best of Jim Hance", new DateTime(2007, 2, 6)));
            // Set the data context for the list box.
            ListBox1.DataContext = MyMusic;

        }
        private void lstCountries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBox.Show("List Item " + (lstCountries.SelectedIndex + 1) + " pressed.");
        }
    }
}