﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.Windows.Media.Imaging;

namespace PhoneApp1.Test
{
    public partial class Page5 : PhoneApplicationPage
    {
        DispatcherTimer timer = new DispatcherTimer();
        int i = 0;
        public Page5()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }
        void timer_Tick(object sender,EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("dem ne:" + i);
            i++;
            if(i==5)
            {
                timer.Stop();
            }
        }
    }
}