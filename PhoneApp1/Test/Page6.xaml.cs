﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
namespace PhoneApp1.Test
{
    

    public partial class Page6 : PhoneApplicationPage
    {
        public class Recording
        {
            public Recording() { }
            public Recording(string artistName, string cdName, DateTime release)
            {
                Artist = artistName;
                Name = cdName;
                ReleaseDate = release;
            }
            public string Artist { get; set; }
            public string Name { get; set; }
            public DateTime ReleaseDate { get; set; }
            // Override the ToString method.
            public override string ToString()
            {
                return Name + " by " + Artist + ", Released: " + ReleaseDate.ToShortDateString();
            }
        }

        public ObservableCollection<Recording> MyMusic = new ObservableCollection<Recording>();
        public Page6()
        {
            InitializeComponent();
            MyMusic.Add(new Recording("Chris Sells", "Chris Sells Live",
                new DateTime(2008, 2, 5)));
            MyMusic.Add(new Recording("Luka Abrus",
               "The Road to Redmond", new DateTime(2007, 4, 3)));
            MyMusic.Add(new Recording("Jim Hance",
               "The Best of Jim Hance", new DateTime(2007, 2, 6)));
            // Set the data context for the list box.
            listbox.DataContext = MyMusic;
          

        }
    }
}