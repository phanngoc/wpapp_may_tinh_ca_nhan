﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO;
using Newtonsoft.Json.Linq;

namespace PhoneApp1.Test
{
    public partial class Page7 : PhoneApplicationPage
    {
        public Page7()
        {
            InitializeComponent();
            //Uri uri = new Uri("http://media.jeff.wilcox.name/blog/Panorama685.png", UriKind.Absolute);
            //imagelogo.Source = new BitmapImage(uri);
            string avatarUri = "http://tracau.vn:1981/WBBcwnwQpV89/trả lời/en/JSON_CALLBACK";
          //  HttpWebRequest request =
          //      (HttpWebRequest)HttpWebRequest.Create(avatarUri);
          //  request.BeginGetResponse(getResponse, request);
            HttpWebRequest WebReq =
              (HttpWebRequest)WebRequest.Create(avatarUri);
            WebReq.Method = "GET";
            WebReq.BeginGetResponse(new AsyncCallback(getWord), WebReq);
        }
        private void getWord(IAsyncResult result)
        {
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            var httpResponse = (HttpWebResponse)webRequest.EndGetResponse(result);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string responseText = streamReader.ReadToEnd();         
                processJson(responseText);
                streamReader.Close();
            }
        }
        void processJson(string result)
        {
            string json = result.Substring(14, result.Length - 16);
          //  System.Diagnostics.Debug.WriteLine(json);
            JObject jsonObject = JObject.Parse(json);
            JArray jsonArray = (JArray)jsonObject["sentences"];
            for( int i=0 ;i<jsonArray.Count;i++)
            {
              string res = (String)jsonArray[i]["fields"]["en"];
              System.Diagnostics.Debug.WriteLine(res);
            }
        }
        void getResponse(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if(request != null)
            {
                try{
                    WebResponse response = request.EndGetResponse(result);
                    System.Diagnostics.Debug.WriteLine(response.GetResponseStream());
                } 
                catch (WebException e)
                    {

                    }
            }
           
            
        }
    }
}